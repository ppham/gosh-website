import React from 'react';
import './App.css';
import "tailwind/tailwind.css";

function ProductImage() {
  return (
    <div className="ProductImage">
        <img src="./images/product.png" className="productImage" alt="D3D product" />
    </div>
  );
}

export default ProductImage;
